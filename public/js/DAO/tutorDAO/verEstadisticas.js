//variables de sesion
var user = sessionStorage.getItem("user");
var coleccion = sessionStorage.getItem("coleccion");
var userAlumnoT = sessionStorage.getItem("codAlumno");

console.log("el parametro llego = "+userAlumnoT);

//variables de datos de curso
var nomAlumnoT = document.getElementById("nomAlumnoT");


const docRef = db.doc("alumnos/"+userAlumnoT);
//const docRef = db.doc("alumnos/"+arrVariableActual[1]);
docRef.get().then(function (doc) {
    if (doc.exists){
        var ape = doc.data().apellido;
        nomAlumnoT.innerHTML = ape.toUpperCase()+" "+doc.data().nombre;

        var Bim1 = doc.data().bim1;
        var arrBim1 = Bim1.split("-");
        var Bim2 = doc.data().bim2;
        var arrBim2 = Bim2.split("-");
        var Bim3 = doc.data().bim3;
        var arrBim3 = Bim3.split("-");
        var Bim4 = doc.data().bim4;
        var arrBim4 = Bim4.split("-");

        var promBim1 = 0.0;
        var promBim2 = 0.0;
        var promBim3 = 0.0;
        var promBim4 = 0.0;

        console.log("las notas bim1: "+Bim1);
        console.log("las notas bim2: "+Bim2);
        console.log("las notas bim3: "+Bim3);
        console.log("las notas bim4: "+Bim4);


        for (var i=0;i<9;i++){
          promBim1 = promBim1 + parseFloat(arrBim1[i]);
          promBim2 = promBim2 + parseFloat(arrBim2[i]);
          promBim3 = promBim3 + parseFloat(arrBim3[i]);
          promBim4 = promBim4 + parseFloat(arrBim4[i]);
        }

        promBim1 = promBim1/10;
        promBim2 = promBim2/10;
        promBim3 = promBim3/10;
        promBim4 = promBim4/10;

        var nf1 = Math.round(promBim1);
        var nf2 = Math.round(promBim2);
        var nf3 = Math.round(promBim3);
        var nf4 = Math.round(promBim4);

        console.log("las notas bim1: "+nf1);
        console.log("las notas bim2: "+nf2);
        console.log("las notas bim3: "+nf3);
        console.log("las notas bim4: "+nf4);

        Morris.Line({
          element: 'graph-nota-alum-tutor',
          data: [
            {"elapsed": "1er Bimestre", "value": nf1},
            {"elapsed": "2do Bimestre", "value": nf2},
            {"elapsed": "3er Bimestre", "value": nf3},
            {"elapsed": "4to Bimestre", "value": nf4}
          ],
          xkey: 'elapsed',
          ykeys: ['value'],
          labels: ['Nota'],
          resize: true,
          padding: 50,
          parseTime: false
        });


        console.log("Temprano: "+doc.data().temprano);
        console.log("Tardanzas: "+doc.data().tarde);
        console.log("Faltas: "+doc.data().falta);
        var totalasist = parseFloat(doc.data().temprano) + parseFloat(doc.data().tarde) + parseFloat(doc.data().falta);
        console.log("Total: "+totalasist);
        var temprano = (parseFloat(doc.data().temprano)*100)/parseFloat(totalasist);
        var tarde = (parseFloat(doc.data().tarde)*100)/parseFloat(totalasist);
        var falta = (parseFloat(doc.data().falta)*100)/parseFloat(totalasist);
        var te = Number((temprano).toFixed(1));
        var ta = Number((tarde).toFixed(1));
        var fa = Number((falta).toFixed(1));
        console.log("%Temprano: "+te);
        console.log("%Tardanzas: "+ta);
        console.log("%Faltas: "+fa);

        Morris.Donut({
          element: 'graph-asistencia-alum-tutor',
          data: [
            {value: te, label: 'Asistencias'},
            {value: ta, label: 'Tardanzas'},
            {value: fa, label: 'Faltas'}
          ],
          backgroundColor: 'rgba(0, 0, 0, 0.0)',
          resize: true,
          colors: [
            '#32CD32',
            '#FECE4D',
            '#FE4D4D',
          ],
          formatter: function (x) { return x + "%"}
        }).on('click', function(i, row){
          console.log(i, row);
        });


    }else {
        console.log("No existe el documento");
    }
}).catch(function (error) {
    console.log("Error al buscar el documento:",error);
});
