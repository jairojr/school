//variables de sesion
var user = sessionStorage.getItem("user");
var coleccion = sessionStorage.getItem("coleccion");

//variables de datos personales
var dniP = document.getElementById("prof-dni");
var nombreP = document.getElementById("prof-name");
var apellidoP = document.getElementById("prof-lastname");
var telefonoP = document.getElementById("prof-phone");
var emailP = document.getElementById("prof-email");
var gradoP = document.getElementById("prof-grade");
var seccionP = document.getElementById("prof-section");
var unameP = document.getElementById("prof-uname");

//listar tutores del mismo grado que el profesor
const docRef = db.doc("tutores/"+user);
docRef.get().then(function (doc) {
    if (doc.exists){
        var Pgrado = doc.data().grado;
        var Pseccion = doc.data().seccion;
        //info del tutor
        db.collection("profesores").where("grado", "==", `${Pgrado}`).get()
            .then(function(querySnapshot) {
                querySnapshot.forEach(function(dc) {
                    if (dc.data().seccion == Pseccion){

                      dniP.innerHTML = `<p>Número de DNI</p>
                                         <input type="text" class="material-control tooltips-general" value="${dc.data().dni}" readonly="readonly">
                                         <span class="highlight"></span>
                                         <span class="bar"></span>`;
                      nombreP.innerHTML = `<p>Nombres</p>
                                           <input type="text" class="material-control tooltips-general" value="${dc.data().nombre}" readonly="readonly">
                                           <span class="highlight"></span>
                                           <span class="bar"></span>`;
                      apellidoP.innerHTML = `<p>Apellidos</p>
                                             <input type="text" class="material-control tooltips-general" value="${dc.data().apellido}" readonly="readonly">
                                             <span class="highlight"></span>
                                             <span class="bar"></span>`;
                      telefonoP.innerHTML = `<p>Teléfono</p>
                                             <input type="text" class="material-control tooltips-general" value="${dc.data().telefono}" readonly="readonly">
                                             <span class="highlight"></span>
                                             <span class="bar"></span>`;
                      emailP.innerHTML = `<p>Correo Electronico</p>
                                          <input type="text" class="material-control tooltips-general" value="${dc.data().email}" readonly="readonly">
                                          <span class="highlight"></span>
                                          <span class="bar"></span>`;
                      gradoP.innerHTML = `<p>Grado</p>
                                          <input type="text" class="material-control tooltips-general" value="${dc.data().grado}º" readonly="readonly">
                                          <span class="highlight"></span>
                                          <span class="bar"></span>`;
                      seccionP.innerHTML = `<p>Sección</p>
                                           <input type="text" class="material-control tooltips-general" value="${dc.data().seccion}" readonly="readonly">
                                           <span class="highlight"></span>
                                           <span class="bar"></span>`;
                    }
                    // doc.data() is never undefined for query doc snapshots
                });
            })
            .catch(function(error) {
                console.log("Error getting documents: ", error);
            });

    }else {
        console.log("No existe el documento");
    }
}).catch(function (error) {
    console.log("Error al buscar el documento:",error);
});
