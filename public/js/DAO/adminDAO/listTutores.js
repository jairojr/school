//variables de sesion
var user = sessionStorage.getItem("user");
var coleccion = sessionStorage.getItem("coleccion");

//variables del id de la tabla
var tTutores = document.getElementById("tablaTutores");
//carga inicial
tTutores.innerHTML = `<div class="div-table" style="margin:0 !important;">
                    <div class="div-table-row div-table-row-list" style="background-color:#DFF0D8; font-weight:bold;">
                        <div class="div-table-cell" style="width: 6%;">#</div>
                        <div class="div-table-cell" style="width: 15%;">DNI</div>
                        <div class="div-table-cell" style="width: 15%;">Grado - Sección</div>
                        <div class="div-table-cell" style="width: 15%;">Apellidos</div>
                        <div class="div-table-cell" style="width: 15%;">Nombres</div>
                        <div class="div-table-cell" style="width: 12%;">Teléfono</div>
                        <div class="div-table-cell" style="width: 18%;">E-mail</div>
                    </div>
                </div>`;

//cantidad de profesores
db.collection("tutores").get()
    .then(function(querySnapshot) {
        num = 0;
        querySnapshot.forEach(function(dc) {
            num = num + 1;
            gs = dc.data().grado+"° - "+dc.data().seccion;
            tTutores.innerHTML += `<div class="table-responsive">
                    <div class="div-table" style="margin:0 !important;">
                        <div class="div-table-row div-table-row-list">
                            <div class="div-table-cell" style="width: 6%;">${num}</div>
                            <div class="div-table-cell" style="width: 15%;">${dc.data().dni}</div>
                            <div class="div-table-cell" style="width: 15%;">${gs}</div>
                            <div class="div-table-cell" style="width: 15%;">${dc.data().apellido}</div>
                            <div class="div-table-cell" style="width: 15%;">${dc.data().nombre}</div>
                            <div class="div-table-cell" style="width: 12%;">${dc.data().telefono}</div>
                            <div class="div-table-cell" style="width: 18%;">${dc.data().email}</div>
                        </div>
                    </div>
                </div>`;
            console.log("i="+num);
        });
    })
    .catch(function(error) {
        console.log("Error getting documents: ", error);
    });