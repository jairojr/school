var tAlumnos = document.getElementById("tablaAlumnos");

function primero(grade) {
    tAlumnos.innerHTML = ``;
    tAlumnos.innerHTML = `<h2 class="text-center all-tittles">listado de alumnos</h2>
            <div class="table-responsive">
                <div class="div-table" style="margin:0 !important;">
                    <div class="div-table-row div-table-row-list" style="background-color:#DFF0D8; font-weight:bold;">
                        <div class="div-table-cell" style="width: 6%;">#</div>
                        <div class="div-table-cell" style="width: 18%;">DNi</div>
                        <div class="div-table-cell" style="width: 18%;">Apellidos</div>
                        <div class="div-table-cell" style="width: 18%;">Nombres</div>
                        <div class="div-table-cell" style="width: 18%;">Sección</div>
                        <div class="div-table-cell" style="width: 9%;">Actualizar</div>
                        <div class="div-table-cell" style="width: 9%;">Eliminar</div>
                    </div>
                </div>
            </div>`;

            db.collection("alumnos").where("grado", "==", grade).get()
                .then(function(querySnapshot) {
                    num = 0;
                    querySnapshot.forEach(function(dc) {
                            num = num + 1;
                        tAlumnos.innerHTML += `<div class="table-responsive">
                <div class="div-table" style="margin:0 !important;">
                    <div class="div-table-row div-table-row-list">
                        <div class="div-table-cell" style="width: 6%;">${num}</div>
                        <div class="div-table-cell" style="width: 18%;">${dc.data().dni}</div>
                        <div class="div-table-cell" style="width: 18%;">${dc.data().apellido}</div>
                        <div class="div-table-cell" style="width: 18%;">${dc.data().nombre}</div>
                        <div class="div-table-cell" style="width: 18%;">${dc.data().seccion}</div>
                        <div class="div-table-cell" style="width: 9%;">
                            <button class="btn btn-success"><i class="zmdi zmdi-refresh"></i></button>
                        </div>
                        <div class="div-table-cell" style="width: 9%;">
                            <button class="btn btn-danger"><i class="zmdi zmdi-delete"></i></button>
                        </div>
                    </div>
                </div>
            </div>`;
                            console.log("i="+num);
                    });
                })
                .catch(function(error) {
                    console.log("Error getting documents: ", error);
                });
}