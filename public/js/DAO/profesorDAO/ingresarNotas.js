//variables de sesion
var user = sessionStorage.getItem("user");
var coleccion = sessionStorage.getItem("coleccion");
var userAlumno = sessionStorage.getItem("codAlum");

console.log("el parametro llego = "+userAlumno);

//variables de datos de curso
var nomAlumno = document.getElementById("nomAlumno");
var tablaNotas = document.getElementById("tablaNotas");
tablaNotas.innerHTML = `<h2 class="text-center all-tittles">Notas del Ciclo Escolar</h2>
            <div class="table-responsive">
                <div class="div-table" style="margin:0 !important;">
                    <div class="div-table-row div-table-row-list" style="background-color:#FF8601; font-weight:bold; color:#FFF;">
                        <div class="div-table-cell" style="width: 35%;">Curso</div>
                        <div class="div-table-cell" style="width: 15%;">1er Bimestre</div>
                        <div class="div-table-cell" style="width: 15%;">2do Bimestre</div>
                        <div class="div-table-cell" style="width: 15%;">3er Bimestre</div>
                        <div class="div-table-cell" style="width: 15%;">4to Bimestre</div>
                    </div>
                </div>
            </div>`;

var cursos = ["Lengua","Comprensión Lectora","Ciencia","C.Social","Educaion Fisica","Religión","Ingles","Artes Plásticas","Música"];
const docRef = db.doc("alumnos/"+userAlumno);
//const docRef = db.doc("alumnos/"+arrVariableActual[1]);
docRef.get().then(function (doc) {
    if (doc.exists){
        var ape = doc.data().apellido;
        nomAlumno.innerHTML = ape.toUpperCase()+" "+doc.data().nombre;

        var Bim1 = doc.data().bim1;
        var arrBim1 = Bim1.split("-");
        var Bim2 = doc.data().bim2;
        var arrBim2 = Bim2.split("-");
        var Bim3 = doc.data().bim3;
        var arrBim3 = Bim3.split("-");
        var Bim4 = doc.data().bim4;
        var arrBim4 = Bim4.split("-");

        console.log("las notas bim1: "+Bim1);
        console.log("las notas bim2: "+Bim2);
        console.log("las notas bim3: "+Bim3);
        console.log("las notas bim4: "+Bim4);

        for (var i=0;i<9;i++){
            tablaNotas.innerHTML += `<div class="table-responsive">
                <div class="div-table" style="margin:0 !important;">
                    <div class="div-table-row div-table-row-list">
                        <div class="div-table-cell" style="width: 35%;">${cursos[i]}</div>
                        <div class="div-table-cell" style="width: 15%; padding:5px;">
                          <input type="number" class="material-control tooltips-general" style="text-align:center" pattern="/^-?\\d+\\.?\\d*$/" onKeyPress="if(this.value.length==2) return false;" placeholder="${arrBim1[i]}" min="0" max="20" maxlength="2" data-toggle="tooltip" data-placement="top" title="Ingrese la nota">
                          <span class="highlight"></span>
                          <span class="bar"></span>
                        </div>
                        <div class="div-table-cell" style="width: 15%; padding:5px;">
                          <input type="number" class="material-control tooltips-general" style="text-align:center" pattern="/^-?\\d+\\.?\\d*$/" onKeyPress="if(this.value.length==2) return false;" placeholder="${arrBim2[i]}" min="0" max="20" maxlength="2" data-toggle="tooltip" data-placement="top" title="Ingrese la nota">
                          <span class="highlight"></span>
                          <span class="bar"></span>
                        </div>
                        <div class="div-table-cell" style="width: 15%; padding:5px;">
                          <input type="number" class="material-control tooltips-general" style="text-align:center" pattern="/^-?\\d+\\.?\\d*$/" onKeyPress="if(this.value.length==2) return false;" placeholder="${arrBim3[i]}" min="0" max="20" maxlength="2" data-toggle="tooltip" data-placement="top" title="Ingrese la nota">
                          <span class="highlight"></span>
                          <span class="bar"></span>
                        </div>
                        <div class="div-table-cell" style="width: 15%; padding:5px;">
                          <input type="number" class="material-control tooltips-general" style="text-align:center" pattern="/^-?\\d+\\.?\\d*$/" onKeyPress="if(this.value.length==2) return false;" placeholder="${arrBim4[i]}" min="0" max="20" maxlength="2" data-toggle="tooltip" data-placement="top" title="Ingrese la nota">
                          <span class="highlight"></span>
                          <span class="bar"></span>
                        </div>
                    </div>
                </div>
            </div>`;

        }
        tablaNotas.innerHTML += '<div class="col-xs-12" style="margin-top:30px;">\n' +
            '                 <p class="text-center">\n' +
            '                     <button type="reset" class="btn btn-info" style="margin-right: 20px;"><i class="zmdi zmdi-roller"></i> &nbsp;&nbsp; Cancelar</button>\n' +
            '                     <button type="submit" class="btn btn-primary"><i class="zmdi zmdi-floppy"></i> &nbsp;&nbsp; Guardar</button>\n' +
            '                 </p>\n' +
            '             </div>';

        //muestra datos de notas

    }else {
        console.log("No existe el documento");
    }
}).catch(function (error) {
    console.log("Error al buscar el documento:",error);
});

