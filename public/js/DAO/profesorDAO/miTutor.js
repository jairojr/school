//variables de sesion
var user = sessionStorage.getItem("user");
var coleccion = sessionStorage.getItem("coleccion");

//variables de datos personales
var dniT = document.getElementById("tutor-dni");
var nombreT = document.getElementById("tutor-name");
var apellidoT = document.getElementById("tutor-lastname");
var telefonoT = document.getElementById("tutor-phone");
var emailT = document.getElementById("tutor-email");
var gradoT = document.getElementById("tutor-grade");
var seccionT = document.getElementById("tutor-section");
var unameT = document.getElementById("tutor-uname");

//listar tutores del mismo grado que el profesor
const docRef = db.doc("profesores/"+user);
docRef.get().then(function (doc) {
    if (doc.exists){
        var Pgrado = doc.data().grado;
        var Pseccion = doc.data().seccion;
        //info del tutor
        db.collection("tutores").where("grado", "==", `${Pgrado}`).get()
            .then(function(querySnapshot) {
                querySnapshot.forEach(function(dc) {
                    if (dc.data().seccion == Pseccion){

                      dniT.innerHTML = `<p>Número de DNI</p>
                                         <input type="text" class="material-control tooltips-general" value="${dc.data().dni}" readonly="readonly">
                                         <span class="highlight"></span>
                                         <span class="bar"></span>`;
                      nombreT.innerHTML = `<p>Nombres</p>
                                           <input type="text" class="material-control tooltips-general" value="${dc.data().nombre}" readonly="readonly">
                                           <span class="highlight"></span>
                                           <span class="bar"></span>`;
                      apellidoT.innerHTML = `<p>Apellidos</p>
                                             <input type="text" class="material-control tooltips-general" value="${dc.data().apellido}" readonly="readonly">
                                             <span class="highlight"></span>
                                             <span class="bar"></span>`;
                      telefonoT.innerHTML = `<p>Teléfono</p>
                                             <input type="text" class="material-control tooltips-general" value="${dc.data().telefono}" readonly="readonly">
                                             <span class="highlight"></span>
                                             <span class="bar"></span>`;
                      emailT.innerHTML = `<p>Correo Electronico</p>
                                          <input type="text" class="material-control tooltips-general" value="${dc.data().email}" readonly="readonly">
                                          <span class="highlight"></span>
                                          <span class="bar"></span>`;
                      gradoT.innerHTML = `<p>Grado</p>
                                          <input type="text" class="material-control tooltips-general" value="${dc.data().grado}º" readonly="readonly">
                                          <span class="highlight"></span>
                                          <span class="bar"></span>`;
                      seccionT.innerHTML = `<p>Sección</p>
                                           <input type="text" class="material-control tooltips-general" value="${dc.data().seccion}" readonly="readonly">
                                           <span class="highlight"></span>
                                           <span class="bar"></span>`;
                    }
                    // doc.data() is never undefined for query doc snapshots
                });
            })
            .catch(function(error) {
                console.log("Error getting documents: ", error);
            });

    }else {
        console.log("No existe el documento");
    }
}).catch(function (error) {
    console.log("Error al buscar el documento:",error);
});
