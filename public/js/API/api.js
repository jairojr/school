// ******  REGISTRAR ASISTENCIA
var firebaseConfig = {
    apiKey: "AIzaSyAM0s2JbWKZe86EWcyKbiH2K-8aQkZFxoA",
    authDomain: "school-d0254.firebaseapp.com",
    databaseURL: "https://school-d0254.firebaseio.com",
    projectId: "school-d0254",
    storageBucket: "school-d0254.appspot.com",
    messagingSenderId: "1070440942120",
    appId: "1:1070440942120:web:b522032f70c4da2df2031d",
    measurementId: "G-WQCX7HR67R"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
const db = firebase.firestore();

function marcar() {
    var id = document.getElementById("ident").value;
    var documento = ["15200211","15200219","16200090","17200280","16200259","15200208","00214659"];
    var coleccion = ["admin","tutores","profesores","profesores","alumnos","alumnos","alumnos"];

    console.log("coleccion->"+coleccion[id]+"-"+"documento->"+documento[id]);

    var time = new Date();
    console.log("dia del mes: "+time.getDate());
    var mes = time.getMonth()+1;
    console.log("el mes: "+mes);
//*************  captura de los datos
    db.doc(coleccion[id]+"/"+documento[id]).get().then(function (doc) {
        if (doc.exists){
            var fecha = doc.data().fecha+"/"+"2019-"+mes+"-"+time.getDate();
            var ingreso = doc.data().ingreso+"-"+time.getHours()+":"+time.getMinutes();
            var tarde;
            var temprano;
            if ((time.getHours() > 9)&&(time.getMinutes() > 15)){
                tarde = doc.data().tarde+1;
                temprano = doc.data().temprano;
            }else {
                temprano = doc.data().temprano+1;
                tarde = doc.data().tarde;
            }
            if (id=="0"){
                //se almacena en la bd del admin
                db.doc(coleccion[id]+"/"+documento[id]).set({

                    apellido: doc.data().apellido,
                    contrasena: doc.data().contrasena,
                    dni: doc.data().dni,
                    email: doc.data().email,
                    falta: doc.data().falta,
                    fecha: fecha,
                    ingreso: ingreso,
                    nombre: doc.data().nombre,
                    tarde: tarde,
                    telefono: doc.data().telefono,
                    temprano: temprano,
                    usuario: doc.data().usuario
                }).then(function() {
                    console.log("documento correctamente escrito");
                }).catch(function(error) {
                    alert('Lo sentimos, ha ocurrido un error');
                    console.error("Error adding document: ", error);
                });
                console.log("Persona encontrada: "+doc.data().nombre);
            }else if (id=="1"){
                //se almacena en la bd del tutor
                db.doc(coleccion[id]+"/"+documento[id]).set({

                    apellido: doc.data().apellido,
                    contrasena: doc.data().contrasena,
                    dni: doc.data().dni,
                    email: doc.data().email,
                    falta: doc.data().falta,
                    fecha: fecha,
                    grado: doc.data().grado,
                    ingreso: ingreso,
                    nombre: doc.data().nombre,
                    seccion: doc.data().seccion,
                    tarde: tarde,
                    telefono: doc.data().telefono,
                    temprano: temprano,
                    usuario: doc.data().usuario
                }).then(function() {
                    console.log("documento correctamente escrito");
                }).catch(function(error) {
                    alert('Lo sentimos, ha ocurrido un error');
                    console.error("Error adding document: ", error);
                });
                console.log("Persona encontrada: "+doc.data().nombre);
            }else if (id=="2" || id=="3"){
                //se almacena en la bd del profesor
                db.doc(coleccion[id]+"/"+documento[id]).set({

                    apellido: doc.data().apellido,
                    contrasena: doc.data().contrasena,
                    dni: doc.data().dni,
                    email: doc.data().email,
                    falta: doc.data().falta,
                    fecha: fecha,
                    grado: doc.data().grado,
                    ingreso: ingreso,
                    nombre: doc.data().nombre,
                    seccion: doc.data().seccion,
                    tarde: tarde,
                    telefono: doc.data().telefono,
                    temprano: temprano,
                    usuario: doc.data().usuario
                }).then(function() {
                    console.log("documento correctamente escrito");
                }).catch(function(error) {
                    alert('Lo sentimos, ha ocurrido un error');
                    console.error("Error adding document: ", error);
                });
                console.log("Persona encontrada: "+doc.data().nombre);
            }else if (id=="4" || id=="5" || id="6"){
                //se almacena en la bd del alumno
                db.doc(coleccion[id]+"/"+documento[id]).set({

                    apellido: doc.data().apellido,
                    bim1: doc.data().bim1,
                    bim2: doc.data().bim2,
                    bim3: doc.data().bim3,
                    bim4: doc.data().bim4,
                    contrasena: doc.data().contrasena,
                    dni: doc.data().dni,
                    email: doc.data().email,
                    falta: doc.data().falta,
                    fecha: fecha,
                    grado: doc.data().grado,
                    ingreso: ingreso,
                    nombre: doc.data().nombre,
                    seccion: doc.data().seccion,
                    tarde: tarde,
                    telefono: doc.data().telefono,
                    temprano: temprano,
                    usuario: doc.data().usuario
                }).then(function() {
                    console.log("documento correctamente escrito");
                }).catch(function(error) {
                    alert('Lo sentimos, ha ocurrido un error');
                    console.error("Error adding document: ", error);
                });
                console.log("Persona encontrada: "+doc.data().nombre);
            }
        }else {
            console.log("No existe el documento");
        }
    }).catch(function (error) {
        console.log("Error al buscar el documento:",error);
    });

}




