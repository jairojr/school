// Your web app's Firebase configuration
var firebaseConfig = {
    apiKey: "AIzaSyDLrnhkERN4pxl_yUWvFcjtJVDNoqdEMoI",
    authDomain: "auth-93ead.firebaseapp.com",
    databaseURL: "https://auth-93ead.firebaseio.com",
    projectId: "auth-93ead",
    storageBucket: "auth-93ead.appspot.com",
    messagingSenderId: "577479532901",
    appId: "1:577479532901:web:7aa489875fd305c9f4818c",
    measurementId: "G-0FNHT3WDJ7"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
firebase.analytics();

const txtEmail = document.getElementById("txtEmail");
const txtPassword = document.getElementById("txtPassword");
const btnLogin = document.getElementById("btnLogin");
const btnSignUp = document.getElementById("btnSignUp");
const btnLogout = document.getElementById("btnLogout");

//evento login
btnLogin.addEventListener('click',ev => {
    //captura de los datos
    const email = txtEmail.value;
    const pass = txtPassword.value;
    //constante firebase
    const auth = firebase.auth();
    //sign in
    const promise = auth.signInWithEmailAndPassword(email,pass);
    promise.catch(e => console.log(e.message));
});

//evento signup
btnSignUp.addEventListener('click',ev => {
    //captura de los datos
    const email = txtEmail.value;
    const pass = txtPassword.value;
    //constante firebase
    const auth = firebase.auth();
    //sign in
    const promise = auth.createUserWithEmailAndPassword(email,pass);
    promise.catch(e => console.log(e.message));
});

//add a realtime listener
firebase.auth().onAuthStateChanged(firebaseUser =>{
    if (firebaseUser){
        console.log(firebaseUser);
    }else {
        console.log("no logged in");
    }
});

//evento logout
btnLogout.addEventListener('click',ev => {
    firebase.auth().signOut();
});