// Your web app's Firebase configuration
var firebaseConfig = {
    apiKey: "AIzaSyDLrnhkERN4pxl_yUWvFcjtJVDNoqdEMoI",
    authDomain: "auth-93ead.firebaseapp.com",
    databaseURL: "https://auth-93ead.firebaseio.com",
    projectId: "auth-93ead",
    storageBucket: "auth-93ead.appspot.com",
    messagingSenderId: "577479532901",
    appId: "1:577479532901:web:7aa489875fd305c9f4818c",
    measurementId: "G-0FNHT3WDJ7"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
firebase.analytics();

//login
var provider = new firebase.auth.GoogleAuthProvider();

$('#login').click(function () {
    firebase.auth().signInWithPopup(provider).then(function(result) {
        //cuando ya sucedio el permiso
        guardar(result.user);
        $('#login').hide();
        $('#root').append("<img src='"+result.user.photoURL+"'>");
        console.log(result.user);
    });
});

//funcion q guarda automaticamente
function guardar(user){
    var usuario = {
        uid:user.uid,
        nombre:user.displayName,
        email:user.email,
        foto:user.photoURL
    }
    //push para no grabar en toda la rama si no sustituir
    //firebase.database().ref("usuario/"+user.uid).push(usuario)
    firebase.database().ref("usuario/"+user.uid).set(usuario)
}

//escribir en la BD
$('#guardar').click(function () {
    firebase.database().ref("mascota").set({
        nombre:"Bliss",
        edad:"14",
        sexo:"Mucho"
    })
});

//leer de la BD
firebase.database().ref("usuario").on("child_adderd",function (s) {
    var user = s.val();
    $('#root').append("<img src='"+user.foto+"'>");
});