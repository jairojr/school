// Your web app's Firebase configuration
var firebaseConfig = {
    apiKey: "AIzaSyDLrnhkERN4pxl_yUWvFcjtJVDNoqdEMoI",
    authDomain: "auth-93ead.firebaseapp.com",
    databaseURL: "https://auth-93ead.firebaseio.com",
    projectId: "auth-93ead",
    storageBucket: "auth-93ead.appspot.com",
    messagingSenderId: "577479532901",
    appId: "1:577479532901:web:7aa489875fd305c9f4818c",
    measurementId: "G-0FNHT3WDJ7"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
firebase.analytics();

// Initialize the FirebaseUI Widget using Firebase.
var ui = new firebaseui.auth.AuthUI(firebase.auth());

ui.start('#firebaseui-auth-container', {
    signInOptions: [
        // List of OAuth providers supported.
        firebase.auth.GoogleAuthProvider.PROVIDER_ID,
        firebase.auth.FacebookAuthProvider.PROVIDER_ID,
        firebase.auth.TwitterAuthProvider.PROVIDER_ID,
        firebase.auth.GithubAuthProvider.PROVIDER_ID
    ],
    // Other config options...
});

var uiConfig = {
    callbacks: {
        signInSuccessWithAuthResult: function(authResult, redirectUrl) {
            // User successfully signed in.
            // Return type determines whether we continue the redirect automatically
            // or whether we leave that to developer to handle.
            return true;
        },
        uiShown: function() {
            // The widget is rendered.
            // Hide the loader.
            document.getElementById('loader').style.display = 'none';
        }
    },
    // Will use popup for IDP Providers sign-in flow instead of the default, redirect.
    signInFlow: 'popup',
    signInSuccessUrl: '<url-to-redirect-to-on-success>',
    signInOptions: [
        // Leave the lines as is for the providers you want to offer your users.
        firebase.auth.GoogleAuthProvider.PROVIDER_ID,
        firebase.auth.FacebookAuthProvider.PROVIDER_ID,
        firebase.auth.TwitterAuthProvider.PROVIDER_ID,
        firebase.auth.GithubAuthProvider.PROVIDER_ID,
        firebase.auth.EmailAuthProvider.PROVIDER_ID,
        firebase.auth.PhoneAuthProvider.PROVIDER_ID
    ],
    // Terms of service url.
    tosUrl: '<your-tos-url>',
    // Privacy policy url.
    privacyPolicyUrl: '<your-privacy-policy-url>'
};

// The start method will wait until the DOM is loaded.
ui.start('#firebaseui-auth-container', uiConfig);



/*
//login
var provider = new firebase.auth.GoogleAuthProvider();

$('#login').click(function () {
    firebase.auth().signInWithPopup(provider).then(function(result) {
        //cuando ya sucedio el permiso
        guardar(result.user);
        $('#login').hide();
        $('#root').append("<img src='"+result.user.photoURL+"'>");
        console.log(result.user);
    });
});

//funcion q guarda automaticamente
function guardar(user){
    var usuario = {
        uid:user.uid,
        nombre:user.displayName,
        email:user.email,
        foto:user.photoURL
    }
    //push para no grabar en toda la rama si no sustituir
    //firebase.database().ref("usuario/"+user.uid).push(usuario)
    firebase.database().ref("usuario/"+user.uid).set(usuario)
}

//escribir en la BD
$('#guardar').click(function () {
    firebase.database().ref("mascota").set({
        nombre:"Bliss",
        edad:"14",
        sexo:"Mucho"
    })
});

//leer de la BD
firebase.database().ref("usuario").on("child_adderd",function (s) {
    var user = s.val();
    $('#root').append("<img src='"+user.foto+"'>");
});

 */